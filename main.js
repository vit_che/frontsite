$(document).ready(function() {

    console.log('Cookie ' + document.cookie);

    // function getCookie(name) {
    //     var matches = document.cookie.match(new RegExp(
    //       "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    //     ));
    //     return matches ? decodeURIComponent(matches[1]) : undefined;
    // };
   

    // $.ajaxSetup({ headers: { 'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") } });

    getContent();

    function getContent(){
        $.ajax({       
            type: "GET",
            url: 'http://backsite/getArticles',
            // url: 'http://127.0.0.1:8000/getArticles',
    
            success: function(data){

                console.log(data);
    
                // console.log(getCookie("XSRF-TOKEN"));
                // alert(document.cookie);
                
                $('tr.del').remove();
    
                data.forEach( function(el){
                    $('#articles_list_tr').append('<tr class="del table-warning show_event" data-id="'+ el.id +'"><td>' + 
                    el.id + '</td><td><a class="show_event" href="#">'+ el.title + '</a></td><td>' + 
                    el.content + '</td><td><button class="del_article" data-id="'+ 
                    el.id +'">Delete</button></td></tr>')
                });
                $('.del_article').on('click', delArticle); // DEL button
            },
            error: function(data){
                // console.log('No auth');               
            }
        });
    }

    /* delete article function */
    function delArticle(data){
        var id = $(this).data('id');
        console.log(id); // test
        $.ajax({       
            type: "POST",
            data: {
                id: id
            },
            url: 'http://backsite/delArticle',
            success: function(data){
                console.log('success delete');  
                getContent();             
            },
            error: function(data){
                console.log('error');               
            }
        });
    };


    /* button for call Login form */
    $('#login_btn').click(function () {
        $('#login').dialog('open');
    });

    /* login form */
    $( "#login" ).dialog({

        autoOpen: false,
        buttons: [ 
            {
                id: 'enter',
                text: 'Войти',
                click: function() { 
                    var user_name = $('#name');
                    var user_password = $('#password');

                    // $.ajaxSetup({
                    //     headers: {
                    //         'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") ,
                    //     }
                    // });

                    $.ajax({                        
                        type: "POST",
                        url: "http://backsite/login",
                        data: {
                            name: user_name.val(),
                            password: user_password.val(), 
                        },
                        success: function(data){
                            console.log('Success Login');
                            // console.log(data); //for test
                            // $('#login').dialog('close');
                            // $('#calendar').fullCalendar( 'refetchEvents'  );
                            // location.reload();
                        },
                        error: function(data){
                            console.log('Error Login');       
                            // console.log(data.responseJSON.name);
                            // $('#login').dialog('close');
                            // $('#info').removeClass();
                            // $('#info').addClass('ui-state-highlight');
                            // $('#info').text(data.responseJSON.name);
                            // $('#info').dialog('open');
                        }
                    });
                }
            },
            {   
                id: 'dont',
                text: 'Отмена',
                click: function() { 
                    $('#login').dialog('close');
                }
            }
        ],
        show: {
        effect: "drop",
        duration: 2000
        },

        hide: {
        effect: "drop",
        duration: 2000
        }
    });

    $('#new_article_btn').click(function () {
        $('#article_form').dialog('open');
    });

    /* Article form */
    $( "#article_form" ).dialog({

        autoOpen: false,
        buttons: [ 
            {
                id: 'add_article',
                text: 'Add',
                click: function() { 
                    var title = $('#title');
                    var content = $('#content');

                    // $.ajaxSetup({
                    //     headers: {
                    //         'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") ,
                    //     }
                    // });

                    $.ajax({                        
                        type: "POST",
                        url: 'http://backsite/addArticle',
                        // url: "http://127.0.0.1:8000/addArticle",
                        data: {
                            title: title.val(),
                            content: content.val(), 
                        },
                        crossDomain : true,
                        success: function(data){
                            console.log('Success Add Article');

                            $('#article_form').dialog('close');
                            
                            // console.log(data); //for test
                            // $('#login').dialog('close');
                            // $('#calendar').fullCalendar( 'refetchEvents'  );
                            // location.reload();
                            getContent();
                        },
                        error: function(data){
                            console.log('Error Add Article');       
                            // console.log(data.responseJSON.name);
                            // $('#login').dialog('close');
                            // $('#info').removeClass();
                            // $('#info').addClass('ui-state-highlight');
                            // $('#info').text(data.responseJSON.name);
                            // $('#info').dialog('open');
                        }
                    });
                }
            },
    
            {   
                id: 'dont',
                text: 'Отмена',
                click: function() { 
                    $('#article_form').dialog('close');
                }
            }
        ],
        show: {
        effect: "drop",
        duration: 500
        },

        hide: {
        effect: "drop",
        duration: 500
        }
    });
});
